from django.shortcuts import render, redirect
from todos.models import TodoList, TodoItem


# Create your views here.
def todo_list(request):
    list = TodoList.objects.all()

    context = {"todo_list": list}

    return render(request, "todos/todo_list_list.html")
